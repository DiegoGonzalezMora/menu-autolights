/*------Public parameters------*/
float maxGlow=0.05;
float lightRadio=0.75;
integer onFullBright=FALSE;

/*------Change at your own resk below this line------*/
string autoLights = "OFF";
string lights = "OFF";
integer dialogChannel; 
integer dialogHandle;
integer isOn;
key ownerKey;   
key lastUser;
string user="owner";
list lightList=[];

open_menu(string inputString, list inputList){
    dialogChannel = (integer)llFrand(DEBUG_CHANNEL)*-1;
    dialogHandle = llListen(dialogChannel, "", lastUser, "");
    llDialog(lastUser, inputString, inputList, dialogChannel);
}

close_menu() {
    llListenRemove(dialogHandle);
    dialogHandle = FALSE;
    dialogChannel = (integer)llFrand(DEBUG_CHANNEL)*-1;
}

search(){  
integer count=llGetNumberOfPrims();
    do{
        if(llToUpper(llGetLinkName(count))=="LIGHT") lightList+=[count];
    }while (--count);
}

triggerLights(integer switch){
    integer i=0;
    for(;i<llGetListLength(lightList);i++){
        if(switch){
            llSetLinkPrimitiveParamsFast(llList2Integer(lightList,i),[
                PRIM_POINT_LIGHT, TRUE, <1,1,1>, 1, 6, lightRadio,
                PRIM_GLOW, ALL_SIDES, maxGlow,
                PRIM_FULLBRIGHT, ALL_SIDES, onFullBright]);
                
            lights = "ON";
        }
        else{
            llSetLinkPrimitiveParamsFast(llList2Integer(lightList,i),[
                PRIM_POINT_LIGHT, FALSE, <1,1,1>, 1, 6, lightRadio, 
                PRIM_GLOW, ALL_SIDES, 0.0,
                PRIM_FULLBRIGHT, ALL_SIDES, 0]);
        
            lights = "OFF";
        }
    }
}

default
{
    
    state_entry()
    {             
        search();         
        triggerLights(FALSE);                        
        llSetTimerEvent(10);
        ownerKey = llGetOwner();
        lastUser=ownerKey;
    }
    
    touch_start(integer num_detected)
    {
        lastUser=llDetectedKey(0);
        
        if (user=="owner" && lastUser!=ownerKey ) return;
        
        else if ( user=="group" && !llDetectedGroup(0) && lastUser != ownerKey ) return;
        
        if (dialogHandle) close_menu();
        
        if (dialogHandle) close_menu();
           open_menu(" ",["Menu: "+user, "Light-"+lights, "AutoLight-"+autoLights]);
        
    }
    
    listen(integer channel, string name, key id, string msg){
        if(channel!=dialogChannel) return;
        close_menu();    
        
        if(msg=="AutoLight-ON"){
            autoLights = "OFF";
        }
        
        else if(msg=="AutoLight-OFF"){
            autoLights = "ON";
        }
        
        else if(msg=="Light-ON"){
            if(autoLights == "ON"){
                llOwnerSay("Autolight is OFF");
                autoLights = "OFF";
            }
            triggerLights(FALSE);  
        }
        
        else if(msg=="Light-OFF"){
            if(autoLights == "ON"){
                llOwnerSay("Autolight is OFF");
                autoLights = "OFF";
            }
            triggerLights(TRUE);  
        }
        
        else if(msg=="Menu: owner"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="public";
        }
        
        else if(msg=="Menu: public"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="group";
        }
        
        else if(msg=="Menu: group"){
            if(lastUser!=ownerKey){
                llSay(0,"Owner the owner can change this setting");  
                return;  
            }
            else
                user="owner";
        }        
    }
    
    timer()
    {
        if(autoLights=="ON"){
            vector sun = llGetSunDirection();
            
            //llOwnerSay((string)sun.z);
            
            if(sun.z < 0) triggerLights(TRUE);  
            else triggerLights(FALSE);  
        }
    }
    
    on_rez(integer start_param)
    {
        llResetScript(); 
    }
    
     changed(integer change)
     {
         if (change & (CHANGED_OWNER || CHANGED_LINK)) llResetScript();
     }
}